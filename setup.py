import os

from setuptools import setup, find_packages


def load_requirements():
    """
    Load requirements file and return non-empty, non-comment lines with leading and trailing
    whitespace stripped.
    """
    with open(os.path.join(os.path.dirname(__file__), 'requirements.txt')) as f:
        return [
            line.strip() for line in f
            if line.strip() != '' and not line.strip().startswith('#')
        ]


setup(
    name='meeseeks',
    version='0.1.0',
    packages=find_packages(exclude=['tests']),
    install_requires=load_requirements(),
    entry_points={
        'console_scripts': [
            'meeseeks=meeseeks:main',
        ]
    }
)
